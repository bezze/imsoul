use image;

use image::{
    GenericImage,
};


fn test_image() {

    let mut di = image::DynamicImage::new_rgba8(256, 256);

    for j in 0..256 {

        for i in 0..127 {
            di.put_pixel(i, j, image::Rgba::<u8>([0, 255, 0, 255]));
        }

        for i in 127..256 {
            di.put_pixel(i, j, image::Rgba::<u8>([0, 0, 255, 255]));
        }

    }


}
