use crate::common;
use crate::tree;

use std::f64;
use std::collections::HashMap;

use nalgebra as na;
use image;

use clap::{
    ArgMatches
};

use image::{
    GenericImageView,
    ImageBuffer,
    Rgba,
};

use na::{
    DMatrix,
    DVector,
    // RealField,
    Vector3,
};

use simba::scalar::{
    SubsetOf,
    RealField
};

use common::{
    PosPixVec,
};

use tree::{
    Tree,
    BTNode,
};


#[derive(Debug)]
struct Split<N>
where
    N: RealField + SubsetOf<f64>
{
    pub ppv: PosPixVec,
    pub cov: DMatrix<N>,
    pub eval: N,
    pub evec: DVector<N>
}

impl<N> Split<N>
where
    N: RealField + SubsetOf<f64>
{

    pub fn new(ppv: PosPixVec) -> Self {
        let (cov, eval, evec): (DMatrix<N>, N, DVector<N>) = ppv.class_variance();
        Self { ppv, cov, eval, evec }
    }

}


fn n_splits<N>(n: usize, ppv: PosPixVec) -> Vec<PosPixVec>
where
    N: RealField + SubsetOf<f64>
{

    let root: Split<N> = Split::new(ppv);

    let mut tree = Tree::new(root);

    for _i in 0..(n-1) {

        let hi_var = tree.leaves.iter().max_by(|x, y| {
            let xs = tree.get(x).unwrap();
            let ys = tree.get(y).unwrap();
            xs.data.eval.partial_cmp(&ys.data.eval).unwrap()
        }).unwrap();

        if let Some(node) = tree.map.get(hi_var) {

            let BTNode {data, dir, ..} = node;

            let Split{ppv, cov, eval, evec: normal} = data;

            let avg = ppv.average_dvec();

            let (low, hi): (PosPixVec, PosPixVec) = ppv.plane_split(&normal, &avg);

            tree.add_childs(&dir.clone(), (Split::new(low), Split::new(hi)));

        }

    }

    let splits_vec = tree.leaf_data().drain(0..).map(|s| s.ppv).collect();

    splits_vec
}


pub fn pca(sub_m: &ArgMatches) {

    let number = sub_m.value_of("number").unwrap().parse::<usize>().expect("This is not an integer");
    let img = common::open_image_file(sub_m);

    let ppv = PosPixVec::from_di(&img);

    let mut splits: Vec<PosPixVec> = n_splits::<f64>(number, ppv);

    splits.sort_by(|s1, s2| s2.0.len().cmp(&s1.0.len()));

    let avgs: Vec<Rgba<u8>> = splits.iter().map(|s| s.average_rgb()).collect();


    if sub_m.is_present("out") {
        println!("Writing image file with sample colors...");
        let outfile = sub_m.value_of("out").unwrap();
        // let im_soul = common::imsoul(&avgs[..]);
        let im_soul = common::imsoul_2(&avgs[..]);
        im_soul.save(outfile).unwrap();
    }

    if sub_m.is_present("layer-file") {

        let hashes: Vec<HashMap<(u32, u32), Rgba<u8>>> = splits.iter().map(|s| s.to_hmap()).collect();

        let ib = ImageBuffer::from_fn(img.width(), img.height(), |x, y| {

            let mut p = Rgba([0, 0, 0, 0]);

            for (i, hm) in hashes.iter().enumerate() {
                if hm.contains_key(&(x, y)) {
                    p = avgs[i];
                }
            }

            p

        });

        ib.save(sub_m.value_of("layer-file").unwrap());

    }


}


#[cfg(test)]
mod test {

    use super::*;

}

