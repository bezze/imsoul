extern crate clap;
extern crate nalgebra;
extern crate simba;
extern crate image;
#[macro_use]
extern crate approx;

use clap::{
    Arg,
    App,
    SubCommand,
    ArgMatches,
};

mod pca;
mod median_split;
mod common;
mod tree;
mod color;


fn entry_point(matches: ArgMatches) {

    match matches.subcommand() {
        ("median", Some(sub_m)) => { median_split::median(sub_m) },
        ("pca",    Some(sub_m)) => { pca::pca(sub_m) },
        _                       => {},
    }

}

fn main() {

    let common_args = vec!(
        Arg::with_name("number")
        .short("n")
        .long("number")
        .default_value("1"),
        Arg::with_name("out")
        .short("o")
        .long("out")
        .takes_value(true),
        Arg::with_name("file")
        .short("i")
        .takes_value(true)
        .required(true),
    );

    let matches = App::new("imsoul")
        .version("0.0.1")
        .author("Francisco Bezzecchi <franbcki@gmail.com>")
        .about("Simple program to extract most common colors in an image")
        .subcommand(SubCommand::with_name("median")
            .about("Performs splits using maximum difference and median method")
            .args(&common_args)
        )
        .subcommand(SubCommand::with_name("pca")
            .about("Performs principal component analysis (PCA) on image to extract dominant colors")
            .args(&common_args)
            .arg(Arg::with_name("layer-file")
                .help("After making the splits uses segregation info to save condensed colors image."))
        )
        .get_matches();

    entry_point(matches);

}
