#[derive(Copy, Clone, Debug)]
pub struct ColorRGB {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl ColorRGB {

    pub fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }

    // returns ((index_min, min), (index_max, max), chroma)
    pub fn chroma_sort(self) -> ((usize, u8), (usize, u8), u8) {

        let v = vec![self.r, self.g, self.b];

        let (i_max, v_max) = v.iter().enumerate()
            .max_by(|x, y| {
                let (_, xp) = x;
                let (_, yp) = y;
                xp.cmp(yp)
            }).unwrap();

        let (i_min, v_min) = v.iter().enumerate()
            .min_by(|x, y| {
                let (_, xp) = x;
                let (_, yp) = y;
                xp.cmp(yp)
            }).unwrap();

        ((i_min, v[i_min]), (i_max, v[i_max]), v[i_max] - v[i_min])
    }

    pub fn hsv(self) -> (f32, f32, f32) {

        let Self{r, g, b} = self;
        let (min_pair, max_pair, chroma) = self.chroma_sort();
        let (i_max, v_max) = max_pair;

        let h = {
            if chroma == 0 {
                0f32
            } else {
                let h_prime = match i_max {
                    0 => (g as f32 - b as f32) as f32 / chroma as f32 % 6f32,
                    1 => (b as f32 - r as f32) / chroma as f32 + 2f32,
                    2 => (r as f32 - g as f32) as f32 / chroma as f32 + 4f32,
                    _ => panic!("Wrong number of RGB"),
                };
                h_prime as f32 * 60f32
            }
        };

        let v = v_max as f32 / 255_f32;

        let s = {
            if v == 0f32 {
                0f32
            } else {
                chroma as f32 /  v_max as f32
            }
        };

        (h, s, v)

    }

    fn to_hsv(self) -> ColorHSV {
        let (h, s, v) = self.hsv();
        ColorHSV{ h, s, v }
    }

    pub fn comp(&self) -> ColorRGB {
        let r = 255_u8 - self.r;
        let g = 255_u8 - self.g;
        let b = 255_u8 - self.b;
        ColorRGB { r, g, b }
    }

    /// returns an array of analog colors, incrementally rotated [ang, 2*ang, ..., n-1*ang]
    pub fn analog(&self, ang: f32, n: usize) -> Vec<ColorRGB> {
        let hsv = self.to_hsv();
        let mut vecs: Vec<ColorRGB> = Vec::new();
        for i in 0..n {
            let hsv_i = hsv.hue_rotate((i+1) as f32 * ang);
            vecs.push(hsv_i.to_rgb());
        }
        vecs
    }

}

#[derive(Copy, Clone, Debug)]
pub struct ColorHSV {
    pub h: f32,
    pub s: f32,
    pub v: f32,
}

impl ColorHSV {

    pub fn new(h: f32, s: f32, v: f32) -> Self {
        Self { h, s, v }
    }

    pub fn chroma_f32(self) -> f32 {
        // (255_f32 * self.s * self.v).round() as u8
        255_f32 * self.s * self.v
    }

    pub fn rgb(self) -> (f32, f32, f32) {
        let C = self.chroma_f32();
        let hp = self.h / 60_f32;
        let X = self.chroma_f32() * (1_f32 - (hp % 2_f32 - 1_f32).abs());
        let m = 255_f32 * self.v - self.chroma_f32();

        if C == 0_f32 {
            (0_f32, 0_f32, 0_f32)
        }
        else {
            let (r, g, b) = match hp.trunc() as u8 {
                0 => (C, X, 0_f32),
                1 => (X, C, 0_f32),
                2 => (0_f32, C, X),
                3 => (0_f32, X, C),
                4 => (X, 0_f32, C),
                5 => (C, 0_f32, X),
                _ => panic!("Out of bounds error for angle conversion!")
            };
            (r+m, g+m, b+m)
        }
    }

    pub fn to_rgb(self) -> ColorRGB {
        let (r, g, b) = self.rgb();
        ColorRGB{ r: r as u8, g: g as u8, b: b as u8 }
    }

    pub fn hue_rotate(self, ang: f32) -> Self {
        let new_hue = (self.h + ang) % 360_f32;
        Self { h: new_hue, s: self.s, v: self.v }
    }

    pub fn hue_rotate_inplace(mut self, ang: f32) {
        let new_hue = (self.h + ang) % 360_f32;
        self.h = new_hue;
    }

}
