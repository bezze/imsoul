use std::collections::HashMap;

// struct NodeData {
//     value: f64,
// }

pub type Dir = Vec<u8>;

#[derive(Debug)]
pub struct BTNode<NodeData> {
    p: Option<Dir>,
    l: Option<Dir>,
    r: Option<Dir>,
    pub dir: Dir,
    pub data: NodeData
}

impl<NodeData> BTNode<NodeData> {

    pub fn root(data: NodeData) -> Self {
        Self {
            p: None,
            l: None,
            r: None,
            dir: vec![0],
            data,
        }
    }

    pub fn new(p: Dir, d: Dir, data: NodeData) -> Self {
        // let parent = d.clone().remove(d.len());
        Self {
            p: Some(p),
            l: None,
            r: None,
            dir: d,
            data,
        }
    }


    pub fn parent(&self) -> Option<Dir> {
        self.p.clone()
    }

    pub fn left(&self) -> Option<Dir> {
        self.l.clone()
    }

    pub fn right(&self) -> Option<Dir> {
        self.r.clone()
    }

    pub fn has_childs(&self) -> Option<(Dir, Dir)> {
        if self.r.is_some() && self.l.is_some() {
            // Some((self.l.unwrap().to_owned(), self.r.unwrap().to_owned()))
            let l = self.l.as_ref().unwrap().to_owned();
            let r = self.r.as_ref().unwrap().to_owned();
            Some((l, r))
        }
        else {
            None
        }
    }

    pub fn birth(&mut self) -> (Dir, Dir) {
        let mut ldir = self.dir.clone();
        let mut rdir = self.dir.clone();
        ldir.push(0); rdir.push(1);
        let r = (ldir.clone(), rdir.clone());
        self.l = Some(ldir); self.r = Some(rdir);
        r
    }

}


#[derive(Debug)]
pub struct Tree<T> {
    pub map: HashMap<Dir, BTNode<T>>,
    pub leaves: Vec<Dir>,
}

impl <T> Tree<T> {

    pub fn new(data: T) -> Self {
        let mut map = HashMap::new();
        let dir = vec![0];
        map.insert(dir.clone(), BTNode::root(data));
        Self { map, leaves: vec![dir] }
    }

    pub fn get(&self, dir: &Dir) -> Option<&BTNode<T>> {
        self.map.get(dir)
    }

    pub fn get_mut(&mut self, dir: &Dir) -> Option<&mut BTNode<T>> {
        self.map.get_mut(dir)
    }

    pub fn get_parent(&self, dir: &Dir) -> Option<&BTNode<T>> {
        let btdir = self.map.get(dir).and_then(|b| b.parent());
        if let Some(pdir) = btdir {
            self.map.get(&pdir)
        }
        else {
            None
        }
    }

    pub fn get_childs(&self, dir: &Dir) -> Option<(&BTNode<T>, &BTNode<T>)> {
        if let Some(bt) = self.get(dir) {
            if let Some((dirl, dirr)) = bt.has_childs() {
                return Some((self.get(&dirl).unwrap(), self.get(&dirr).unwrap()))
            }
        }
        None
    }

    fn get_childs_cabeza(&self, dir: &Dir) -> Option<(&BTNode<T>, &BTNode<T>)> {
        let mut dirl = dir.to_owned(); dirl.push(0);
        let mut dirr = dir.to_owned(); dirr.push(1);
        let childl = self.get(&dirl);
        let childr = self.get(&dirr);
        if childl.is_some() && childr.is_some() {
            return Some((childl.unwrap(), childr.unwrap()))
        }
        else {
            None
        }
    }

    pub fn find_leaves (&self, dir: &Dir) -> Vec<Dir> {
        let mut leave_dir: Vec<Dir> = Vec::new();
        for (k, v) in self.map.iter() {
            if let Some(_) = v.has_childs() {
                leave_dir.push(k.clone())
            }
        }
        leave_dir
    }

    pub fn birth(&mut self, dir: &Dir) -> Option<(Dir, Dir)> {
        self.map.get_mut(dir).and_then(|p| {
            Some(p.birth())
        })
    }

    pub fn add_childs(&mut self, dir: &Dir, childs: (T, T)) {
        // println!("before {:?} {:?}", dir, self.leaves);
        let r = self.birth(dir).map(|(ldir, rdir)| {
            let lnode = BTNode::new(dir.clone(), ldir.clone(), childs.0);
            let rnode = BTNode::new(dir.clone(), rdir.clone(), childs.1);
            self.map.insert(ldir.clone(), lnode);
            self.map.insert(rdir.clone(), rnode);
            (ldir, rdir)
        }).map(|(ldir, rdir)| {
            self.leaves.push(ldir);
            self.leaves.push(rdir);
        })
        ;

        self.leaves.sort_unstable();
        let index = self.leaves.binary_search(&dir.clone());
        // println!("after {:?} {:?} {:?}", dir, self.leaves, index);
        self.leaves.remove(index.unwrap());

    }

    pub fn map_leaves<'a, A, F>(&'a self, f: F) -> Vec<A>
        where F: Fn(&'a T) -> A
    {
        self.leaves.iter().map(|d| {
            let n = self.get(d).unwrap();
            f(&n.data)
        }).collect()
    }

    pub fn leaf_data(mut self) -> Vec<T> {

        // self.leaves.drain(0..).map(|d| {
        //     let n = self.map.get(&d).unwrap();
        //     n.data
        // }).collect()

        let mut v = Vec::new();
        // for d in self.leaves.iter() {
        //     v.push(self.map.get(d).unwrap());
        // }

        for (d, n) in self.map.drain() {
            if self.leaves.contains(&d) {
                v.push(n.data);
            }

        }

        // v.iter().map(|n| n.data).collect()

        v
    }

}
