use crate::common;

use clap::{
    ArgMatches
};

use image::{
    GenericImageView,
    ImageBuffer,
    RgbaImage,
    Pixel,
};


#[derive(Copy, Clone, Debug)]
enum ColorEnum {
    Red,
    Green,
    Blue
}

impl ColorEnum {

    fn list() -> [ColorEnum;3] {
        return [ColorEnum::Red, ColorEnum::Green, ColorEnum::Blue];
    }

    fn index(&self) -> usize {
        let index = match &self {
            ColorEnum::Red =>  0,
            ColorEnum::Green =>  1,
            ColorEnum::Blue =>  2,
        };
        index
    }

}


fn get_min_max(color: ColorEnum, pixvec: &Vec<image::Rgba<u8>>) -> (u8, u8) {

    let index = match color {
        ColorEnum::Red =>  0,
        ColorEnum::Green =>  1,
        ColorEnum::Blue =>  2,
    };

    let max = pixvec.into_iter().map(|p| p.to_rgb().0[index]).max().unwrap();
    let min = pixvec.into_iter().map(|p| p.to_rgb().0[index]).min().unwrap();

    return (min, max)

}


fn pick_highest_range(pixvec: &Vec<image::Rgba<u8>>) -> (ColorEnum, u8) {

    let mut range = (ColorEnum::Red, 0u8);

    for t in &ColorEnum::list() {
        let (r_min, r_max) = get_min_max(*t, pixvec);
        let color_range = r_max - r_min;
        if color_range > range.1 {
            range = (*t, color_range)
        }
    }

    range

}


fn sort_using_color(color: ColorEnum, pixvec: &[image::Rgba<u8>]) -> Vec<image::Rgba<u8>> {
    let index = color.index();
    let mut v = pixvec.clone().to_vec();
    v.sort_by(|a, b| a[index].partial_cmp(&b[index]).unwrap());
    v.to_vec()
}


fn split_pixels(pixvec: &Vec<image::Rgba<u8>>) -> (Vec<image::Rgba<u8>>, Vec<image::Rgba<u8>>) {
    let (color, _range) = pick_highest_range(pixvec);
    let sorted = sort_using_color(color, pixvec);

    let length = (sorted.len() as f32 / 2_f32 ) as usize;
    let (low, high) = sorted.split_at(length);

    return (Vec::from(low), Vec::from(high))

}


fn get_pixel_vector(img: &image::DynamicImage) -> Vec<image::Rgba<u8>> {
    let mut pixvec = Vec::new();
    for (_x, _y, p) in img.pixels() {
        pixvec.push(p);
    }
    pixvec
}


fn split_pixel_group(pix_group: Vec<Vec<image::Rgba<u8>>>) ->  Vec<Vec<image::Rgba<u8>>> {
    let mut pixel_groups = Vec::new();
    for pixg in pix_group {
        let (l, h) = split_pixels(&pixg);
        pixel_groups.push(l);
        pixel_groups.push(h);
    }
    pixel_groups
}


fn n_splits(n: usize, pixvec: &Vec<image::Rgba<u8>>) -> Vec<Vec<image::Rgba<u8>>> {
    let mut pixel_groups = Vec::new();
    let (l, h) = split_pixels(&pixvec);
    pixel_groups.push(l);
    pixel_groups.push(h);
    for _i in 1..n {
        pixel_groups = split_pixel_group(pixel_groups);
    }
    pixel_groups
}


fn average_split(split: &Vec<image::Rgba<u8>>) -> image::Rgba<u8> {

    let full_sum = split.into_iter().fold([0usize, 0usize, 0usize, 0usize], |acc, p| {
        let mut out = acc;
        out[0] += p[0] as usize;
        out[1] += p[1] as usize;
        out[2] += p[2] as usize;
        out[3] += p[3] as usize;
        out
    });

    let len = split.len() as f32;

    let avg = [
        (full_sum[0] as f32 / len) as u8,
        (full_sum[1] as f32 / len) as u8,
        (full_sum[2] as f32 / len) as u8,
        (full_sum[3] as f32 / len) as u8,
    ];

    let mean_pixel = image::Rgba::from(avg);

    mean_pixel

}


pub fn median(sub_m: &ArgMatches) {

    let img = common::open_image_file(sub_m);

    let number = sub_m.value_of("number").unwrap().parse::<usize>().expect("This is not an integer");

    let pixel_vector = get_pixel_vector(&img);
    let splits = n_splits(number, &pixel_vector);
    let splits_avg: Vec<image::Rgba<u8>>  = splits.into_iter().map(|x| average_split(&x)).collect();

    let im_soul: RgbaImage = common::imsoul(&splits_avg);

    if sub_m.is_present("out") {
        println!("Writing image file with sample colors...");
        let outfile = sub_m.value_of("out").unwrap();
        im_soul.save(outfile).unwrap();
    }

}
