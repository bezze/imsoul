use std::f64;
use std::collections::HashMap;
use std::ops::{RangeInclusive};

use std::convert::{
    TryInto,
    Into,
};

use std::iter::Iterator;

use image;
use nalgebra as na;

use image::{
    GenericImageView,
    GenericImage,
    DynamicImage,
    Rgba,
    RgbaImage,
    ImageBuffer,
    Pixels,
};

use clap::{
    ArgMatches,
};

use na::{
    RealField,
    DVector,
    DMatrix,
    Vector3,
    SymmetricEigen,
};

use simba::scalar::{
    SubsetOf,
};

use crate::color::{
    ColorRGB,
    ColorHSV
};


fn pixel_to_vec3<N>(p: &Rgba<u8>) -> Vector3<N>
where
    N: RealField + SubsetOf<f64>
{
    Vector3::new(
        na::convert(p[0] as f64),
        na::convert(p[1] as f64),
        na::convert(p[2] as f64),
    )
}


#[derive(Debug)]
pub struct PosPixVec (pub Vec<(u32, u32, Rgba<u8>)>);

impl PosPixVec {

    pub fn new() -> Self {
        Self ( Vec::new() )
    }

    pub fn from_di(di: &DynamicImage) -> Self {
        Self(
            di.pixels().fold(Vec::new(), |mut acc, pixel| {
                let (x, y, p) = pixel;
                acc.push(pixel);
                acc
            })
        )
    }

    pub fn from_ib(ib: ImageBuffer<Rgba<u8>, Vec<u8>>) -> Self {
        Self(
            DynamicImage::ImageRgba8(ib).pixels().fold(Vec::new(), |mut acc, pixel| {
                let (x, y, p) = pixel;
                acc.push(pixel);
                acc
            })
        )
    }

    pub fn average(&self) -> Vec<f64> {

        let len = self.0.len() as f64;

        let full_sum = self.0.iter().fold([0usize, 0usize, 0usize, 0usize], |mut acc, (x, y, p)| {
            acc[0] += p[0] as usize;
            acc[1] += p[1] as usize;
            acc[2] += p[2] as usize;
            acc[3] += p[3] as usize;
            acc
        });

        let avg = vec![
            (full_sum[0] as f64 / len),
            (full_sum[1] as f64 / len),
            (full_sum[2] as f64 / len),
            (full_sum[3] as f64 / len),
        ];

        avg

    }

    pub fn average_n<N>(&self) -> Vec<N>
    where
        N: RealField + SubsetOf<f64>
    {
        self.average().iter().map(|x| na::convert(*x)).collect()
    }

    pub fn average_rgb(&self) -> Rgba<u8> {
        let avg: Vec<u8> = self.average().iter().map(|x| *x as u8).collect();
        Rgba([avg[0], avg[1], avg[2], avg[3]])
    }

    pub fn average_dvec<N>(&self) -> DVector<N>
    where
        N: RealField + SubsetOf<f64>
    {
        DVector::<N>::from_vec(self.average_n()[0..3].to_owned())
    }


    pub fn plane_split<N>(&self, normal: &DVector<N>, point: &DVector<N>) -> (Self, Self)
    where
        N: RealField + SubsetOf<f64>
    {

        let cut = normal.dot(&point);

        let mut high_split = Vec::new();
        let mut low_split = Vec::new();

        self.0.iter().for_each(|(x, y, pixel)| {
            let pvec = pixel_to_vec3(&pixel);
            let proj = pvec.dot(normal);
            if proj > cut {
                high_split.push((*x, *y, *pixel));
            }
            else {
                low_split.push((*x, *y, *pixel));
            }
        });

        (Self(low_split), Self(high_split))

    }

    pub fn covariance<N> (&self) -> DMatrix<N>
    where
        N: RealField + SubsetOf<f64>,
    {
        let size = self.0.len();
        let flat_vec: Vec<N> = self.0.iter().fold(Vec::new(), |mut acc, (_, _, p)| {
            for i in 0..3 { acc.push(na::convert(p[i] as f64)) }
            acc
        });

        // r1 g1 b1 r2 g2 b2 r3 g3 b3 ...

        let m = DMatrix::<N>::from_row_slice(size, 3, &flat_vec[..]).transpose();
        let c_mean = m.column_mean();

        let c = m.ncols();
        let r = m.nrows();
        let c_param: N = na::convert(c as f64);

        let mut cov_raw = Vec::new();
        for (i, row_i) in m.row_iter().enumerate() {
            for (j, row_j) in m.row_iter().enumerate() {
                let op = row_j.dot(&row_i) / c_param - c_mean[i] * c_mean[j];
                cov_raw.push(op);
            }
        }

        let cov = DMatrix::<N>::from_row_slice(r, r, &cov_raw);

        cov

    }

    pub fn to_hmap(&self) -> HashMap<(u32, u32), Rgba<u8>> {
        let mut hash_map: HashMap<(u32, u32), Rgba<u8>>  = HashMap::new();
        self.0.iter().for_each(|(x, y, p)| {
            hash_map.insert((*x, *y), *p);
        });
        hash_map
    }


    pub fn class_variance<N>(&self) -> (DMatrix<N>, N, DVector<N>)
    where
        N: RealField + SubsetOf<f64>
    {

        let cov = self.covariance();

        // Eigenvectors are the columns. Doesn't check for symmetric!!!
        let SymmetricEigen {
            eigenvalues,
            eigenvectors
        } = cov.clone().symmetric_eigen();

        let (eval_l, evec_l) = get_largest_eigenvec(&eigenvalues, &eigenvectors);

        (cov, eval_l, evec_l)

    }


}


impl Into<RgbaImage> for PosPixVec {
    // RgbaImage == ImageBuffer<Rgba<u8>, Vec<u8>>
    fn into(self) -> RgbaImage {
        let len = self.0.len() as u32;
        let pixels: Vec<u8> = self.0.into_iter().map(|(_, _, p)| p).fold(Vec::new(), |mut acc, p| {
            let Rgba([r, g, b, a]) = p;
            acc.push(r); acc.push(g); acc.push(b);
            acc
        });
        let ib: RgbaImage = ImageBuffer::from_vec(3 * len, 1u32, pixels).unwrap();
        ib
    }
}


impl Into<DynamicImage> for PosPixVec {
    // RgbaImage == ImageBuffer<Rgba<u8>, Vec<u8>>
    fn into(self) -> DynamicImage {
        let i: RgbaImage = self.into();
        DynamicImage::ImageRgba8(i)
    }
}


// impl Into<DynamicImage> for PosPixVec {
//     fn into(self) -> DynamicImage {
//         let pixels = self.0.into_iter().map(|(_, _, p)| p).collect();
//         let ib: RgbaImage = ImageBuffer::from_vec(self.0.len() as u32, 1u32, pixels).unwrap();
//         DynamicImage::ImageRgba8(ib)
//     }
// }


fn get_largest_eigenvec<'a, N>(eval: &DVector<N>, evec: &DMatrix<N>) -> (N, DVector<N>)
where
    N: RealField + SubsetOf<f64>,
{

    let mut index = 0;
    let mut max = f64::MIN;
    for (i, e) in eval.iter().enumerate() {
        let e_f64: f64 = na::convert(e.abs());
        if  e_f64 > max {
            index = i;
            max = e_f64;
        }
    }

    (eval[index], evec.column(index).clone_owned())

}


pub fn open_image_file(sub_m: &ArgMatches) -> DynamicImage {
    let file = sub_m.value_of("file").unwrap();
    let img = image::open(file).unwrap();
    img
}


pub fn get_pos_pixel_vector(img: &DynamicImage) -> Vec<(u32, u32, Rgba<u8>)> {
    img.pixels().fold(Vec::new(), |mut acc, x| { acc.push(x); acc })
}


pub fn get_pixel_vector(img: &DynamicImage) -> Vec<image::Rgba<u8>> {
    let mut pixvec = Vec::new();
    for (_x, _y, p) in img.pixels() {
        pixvec.push(p);
    }
    pixvec
}


pub fn average(split: &Vec<Rgba<u8>>) -> Vec<f64> {

    let full_sum = split.into_iter().fold([0usize, 0usize, 0usize, 0usize], |acc, p| {
        let mut out = acc;
        out[0] += p[0] as usize;
        out[1] += p[1] as usize;
        out[2] += p[2] as usize;
        out[3] += p[3] as usize;
        out
    });

    let len = split.len() as f64;

    let avg = vec![
        (full_sum[0] as f64 / len),
        (full_sum[1] as f64 / len),
        (full_sum[2] as f64 / len),
        (full_sum[3] as f64 / len),
    ];

    avg

}


pub fn posvec_average(split: &Vec<Rgba<u8>>) -> Vec<f64> {

    let full_sum = split.into_iter().fold([0usize, 0usize, 0usize, 0usize], |acc, p| {
        let mut out = acc;
        out[0] += p[0] as usize;
        out[1] += p[1] as usize;
        out[2] += p[2] as usize;
        out[3] += p[3] as usize;
        out
    });

    let len = split.len() as f64;

    let avg = vec![
        (full_sum[0] as f64 / len),
        (full_sum[1] as f64 / len),
        (full_sum[2] as f64 / len),
        (full_sum[3] as f64 / len),
    ];

    avg

}


pub fn average_rgb(split: &Vec<image::Rgba<u8>>) -> image::Rgba<u8> {

    let avg: Vec<u8> = average(split).iter().map(|x| *x as u8).collect::<Vec<u8>>();
    let avg_array: &[u8; 4] = &avg[..].try_into().unwrap();

    let mean_pixel = image::Rgba::from(*avg_array);

    mean_pixel
}


fn rgba_to_hex(color: &Rgba<u8>) -> String {
    format!("#{:02X}{:02X}{:02X} {:02X}", color[0], color[1], color[2], color[3])
}

fn rgba(color: &Rgba<u8>) -> String {
    format!("({:03}, {:03}, {:03}, {:03})", color[0], color[1], color[2], color[3])
}

pub fn imsoul(splits_avg: &[Rgba<u8>] ) -> RgbaImage {

    let im_soul: RgbaImage = ImageBuffer::from_fn(64u32 * splits_avg.len() as u32, 64u32, |x, _y| {
        let index: usize = ( x as f32 / 64 as f32 ).trunc() as usize;
        splits_avg[index]
    });

    for c in splits_avg {
        println!("{} {}", rgba_to_hex(c), rgba(c));
    }

    im_soul

}

fn _to_Rgba(c: ColorRGB) -> Rgba<u8> {
    Rgba([c.r, c.g, c.b, 255])
}

impl ColorRGB {
    fn to_Rgba(&self) -> Rgba<u8> {
        Rgba([self.r, self.g, self.b, 255])
    }
}

pub fn imsoul_2(splits_avg: &[Rgba<u8>] ) -> RgbaImage {

    let ang = 15_f32;
    let ana_n = 2;

    let colors: Vec<ColorRGB> = splits_avg.iter().map(|c| ColorRGB::new(c[0], c[1], c[2])).collect();
    let comps: Vec<Rgba<u8>> = colors.iter().map(|c| c.comp().to_Rgba()).collect();
    let analogs: Vec<Vec<Rgba<u8>>> = colors.iter()
        .map(|c| {
            let mut ans = c.analog(ang, ana_n);
            ans.extend_from_slice(&c.analog(-ang, ana_n));
            ans.iter().map(ColorRGB::to_Rgba).collect()
        })
    .collect();

    let height = 64u32 * (1u32 + 1u32 + ana_n as u32 * 2u32);

    fn _analog(a: &Vec<Rgba<u8>>, h: usize) -> Rgba<u8> {
        if h > 1 {
            a[h-2]
        }
        else {
            panic!("We should not be here!");
        }
    }

    let r = RangeInclusive::new(2, 2*ana_n);

    let im_soul: RgbaImage = ImageBuffer::from_fn(64u32 * splits_avg.len() as u32, height, |x, _y| {
        let index: usize = ( x as f32 / 64 as f32 ).trunc() as usize;
        let h: usize = ( _y as f32 / 64 as f32 ).trunc() as usize;
        match h {
            0 => splits_avg[index],
            1 => comps[index],
            r => analogs[index][h-2],
            _ => panic!("We should not be here!")
        }
    });

    for (i, c) in splits_avg.iter().enumerate() {
        println!("main   {:02}: {} {}", i, rgba_to_hex(c), rgba(c));
    }

    for (i, c) in comps.iter().enumerate() {
        println!("comple {:02}: {} {}", i, rgba_to_hex(c), rgba(c));
    }

    let I = analogs.len();
    let J = 2 * ana_n;

    for j in 0..J {
        for i in 0..I {
            let c = &analogs[i][j];
            println!("an {:02}, {:02}: {} {}", i, j, rgba_to_hex(c), rgba(c));
        }
    }

    im_soul

}


#[cfg(test)]
mod test {

    use super::*;

    fn test_image() {
        let mut di = image::DynamicImage::new_rgba8(256, 256);
        for j in 0..256 {
            for i in 0..128 {
                di.put_pixel(i, j, image::Rgba::<u8>([0, 255, 0, 255]));
            }
            for i in 128..256 {
                di.put_pixel(i, j, image::Rgba::<u8>([0, 0, 255, 255]));
            }
        }
        di.save("/tmp/test.png");
    }

    fn test_image_2() -> DynamicImage {
        let mut di = image::DynamicImage::new_rgba8(8, 8);
        for j in 0..8 {
            for i in 0..8 {
                if j > 4 {
                    if i < 4 {
                        di.put_pixel(i, j, image::Rgba::<u8>([0, 255, 0, 255]));
                    }
                    else {
                        di.put_pixel(i, j, image::Rgba::<u8>([0, 0, 255, 255]));
                    }
                }
                else {
                    di.put_pixel(i, j, image::Rgba::<u8>([255, 0, 0, 255]));
                }
            }
        }
        // di.save("./test.2.png");
        di
    }

    #[test]
    fn pos_pix_vec_average() {
        test_image();
        let di: DynamicImage = image::open("/tmp/test.png").unwrap();
        let ppv = PosPixVec::from_di(&di);
        let avg = ppv.average();
        // println!("{:?}", )
        let result = vec![0.0, 127.5, 127.5, 255.0];
        let mut test = true;

        for (i, j) in result.iter().zip(avg.iter()) {
            test = test && relative_eq![i, j];
        }

        assert![test];

    }

    // #[test]
    fn pos_pix_vec_write() {
        let di: DynamicImage = image::open("/tmp/test.png").unwrap();
        let ppv = PosPixVec::from_di(&di);
    }

    #[test]
    fn pos_pix_vec_covariance() {

        let tol = 50f64 * std::f64::EPSILON;

        let di = test_image_2();

        let ppv = PosPixVec::from_di(&di);

        let cov = ppv.covariance();

        let cov_raw = vec![
             15240.234375,   -7620.1171875,  -7620.1171875,
            -7620.1171875,   9906.15234375, -2286.03515625,
            -7620.1171875,  -2286.03515625,  9906.15234375
        ];

        let cov_result = DMatrix::<f64>::from_row_slice(3, 3, &cov_raw);

        assert![cov.relative_eq(&cov_result, tol, tol)];

    }

    #[test]
    fn pos_pix_vec_plane_split() {

        let di = test_image_2();

        let ppv = PosPixVec::from_di(&di);

        let normal = DVector::<f64>::from_row_slice(&vec![1., 0., 0.][..]);
        let point = DVector::<f64>::from_row_slice(&vec![128., 0., 0.][..]);

        let (s1, s2) = ppv.plane_split(&normal, &point);

        println!("s1 {:?} {:?}", s1.average(), vec![0., 127.5, 127.5, 255.]);
        println!("s2 {:?} {:?}", s2.average(), vec![255., 0., 0., 255.]);

        assert_eq![s1.average(), vec![0.  , 127.5, 127.5, 255.]];
        assert_eq![s2.average(), vec![255.,    0.,    0., 255.]];

    }




}
