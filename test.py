import numpy as np
from PIL import Image


a = np.asarray([4, 3, 7, 7, 5, 5, 9, 8, 4, 9])
b = np.asarray([8, 7, 2, 6, 1, 7, 7, 0, 1, 5])
c = np.asarray([8, 2, 4, 4, 1, 3, 9, 3, 6, 4])

# a = np.asarray([4, 3])
# b = np.asarray([8, 7])
# c = np.asarray([8, 2])

m = np.asarray([a,b,c])


def new_m():
    a = []
    for j in range(8):
        for i in range(8):
            if j > 4:
                if i < 4:
                    a += [[0, 255, 0]]
                else:
                    a += [[0, 0, 255]]
            else:
                a += [[255, 0, 0]]
    return np.asarray(a).T


# m = new_m()
m = np.asarray(Image.open("./test.2.png"))
I, J, K = m.shape
l = []
for i in range(I):
    for j in range(J):
        l.append(m[i, j, :])
m = np.asarray(l)
print(m.shape)


N = len(m)
mean = m.mean(axis=0)
# mean = m.mean(axis=1).mean(axis=0)[:3]
# print(m)
print("mean", mean)

cov = []
for i in range(3):
    row = []
    for j in range(3):
        pi = m[i].dot(m[j]) / N - mean[i] * mean[j]
        row.append(pi)
    cov.append(row)

cov = np.asarray(cov)
print("cov\n", cov)

# evals, evecs = np.linalg.eig(cov)
# for i in range(3):
#     print(evals[i], evecs[:, i])
